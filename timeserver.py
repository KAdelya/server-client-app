import socket
import datetime

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM, socket.IPPROTO_TCP)
s.bind(("127.0.0.1", 1303))
while True:
    s.listen(1)
    connection, address = s.accept()
    current_date = datetime.datetime.now().strftime("%d.%m.%Y %H:%M")
    connection.send(f"{current_date}".encode('utf8'))
    connection.close()
